{
   packageOverrides = pkgs:
   {
     myTexLive = with pkgs; texLiveAggregationFun {
       paths = [ texLive texLiveExtra texLiveCMSuper ];
     };
   };
 }
